DESTDIR ?=
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man

VERSION ?= $(shell git rev-parse HEAD)

PROGRAMS = silentct-mac silentct-mon
SRC_DIRS = $(patsubst %,cmd/%,$(PROGRAMS))

all: build man

.PHONY: build
build: $(PROGRAMS)

$(PROGRAMS):
	@mkdir -p build
	go build -o build/$@ $(patsubst %,cmd/%/main.go,$@)

man: $(patsubst %,man-%,$(PROGRAMS))

man-%: build
	help2man \
		--no-info --version-string=$(VERSION) \
		--include=cmd/$*/name.help2man \
		--include=cmd/$*/examples.help2man \
		--include=cmd/$*/see-also.help2man \
		--include=docs/help2man/return-codes.help2man \
		--include=docs/help2man/reporting-bugs.help2man \
		-o build/$*.1 build/$*

# Let this fail if the user did not run make yet (because the build never
# requires sudo, whereas this part may require it depending on location).
.PHONY: install
install:
	@mkdir -p $(DESTDIR)$(BINDIR)
	@mkdir -p $(DESTDIR)$(MANDIR)/man1
	install -m 755 $(patsubst %,build/%,$(PROGRAMS)) $(DESTDIR)$(BINDIR)
	install -m 644 $(patsubst %,build/%.1,$(PROGRAMS)) $(DESTDIR)$(MANDIR)/man1

.PHONY: uninstall
uninstall:
	rm -f $(patsubst %,$(DESTDIR)$(BINDIR)/%,$(PROGRAMS))
	rm -f $(patsubst %,$(DESTDIR)$(MANDIR)/man1/%.1,$(PROGRAMS))

.PHONY: clean
clean:
	rm -rf build
