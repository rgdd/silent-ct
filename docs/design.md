# silentct

This document introduces a silent Certificate Transparency monitor design.

## Setting

We consider a setting where one or more trusted systems request certificates for
a list of domains.  The domains that a system request certificates for may
overlap with the domains of other systems.  For example, there may be two
distinct systems that host and request certificates for `www.example.org`.
Other examples of "systems" that request certificates could include
`jitsi.example.org`, `etherpad.example.org` and `gitlab.example.org`.

The threat we are worried about is certificate mis-issuance.  Due to considering
a multi-system setting with overlapping domains, no single system can be aware
of all legitimately issued certificates for the domains that are being managed.

A certificate is considered mis-issued if it contains:

  1. at least one domain that any of the trusted systems manage _but without any
     of the trusted systems requesting that certificate to be issued_, or
  2. at least one subdomain of the domains that any of the trusted systems
     manage _unless that subdomain is explicitly specified as out of scope_.

The cause of certificate mis-issuance can vary, ranging from BGP and DNS hijacks
to certificate authorities that are coerced, compromised, or actively malicious.

## Goals and non-scope

The goal is to detect certificate mis-issuance.  It is however out of scope to
detect certificate mis-issuance that happened in the past.  In other words, if
the design described herein is put into operation at time `T`, then any
certificate mis-issuance that happened before time `T` is out of scope.  This is
an important constraint that makes it _a lot less costly_ to bootstrap the
monitor.  For example, old certificate backlogs can simply be ignored.

It is also out of scope to detect certificate mis-issuance that targets web
browsers without Certificate Transparency enforcement.  This is because we
cannot get a concise view of all certificates without Certificate Transparency.

To detect certificate mis-issuance, we want to construct a monitor that:

  1. _is easy to self-host_, because you trust yourself or can then (more
     easily) find someone you trust to do the monitoring on your behalf, and
  2. _is silent_, so that there is little or no noise unless certificate
     mis-issuance is actually suspected.  In other words, there should not be a
     notification every time a legitimate certificate is issued or renewed.

The "silent" property helps a lot for system administrators that manage more
than a few certificates.  It also helps in the third-party monitoring setting,
as it would not be more noisy to subscribe to notifications from >1 monitor.

## Assumptions

  - The attacker is unable to control two independent logs that count towards
    the SCT checks in web browsers.  So, we need not worry about split-views and
    can just download the logs while verifying that they are locally consistent.
  - The systems that request certificates start in good states but may be
    compromised sometime in the future.  Detection of certificate mis-issuance
    is then out of scope for all domains that the compromised systems managed.
  - A mis-issued certificate will only be used to target connections from a
    fixed set of IP addresses.  A party that can distinguish between
    certificates that are legitimate and mis-issued will never be targeted.
  - A domain owner notices alerts about suspected certificate mis-issuance.  The
    monitor that generates these alerts is trusted and never compromised.

## Architecture

A monitor downloads all certificates that are issued by certificate authorities
from Certificate Transparency logs.  The exact logs to download is automatically
updated using a list that Google publishes in signed form.  All historical
updates to the list of logs is stored locally in case any issues are suspected.

(It is possible to get INFO output whenever logs are added and removed.  The
default verbosity is however NOTICE, which aims to be as silent as possible.)

To filter out certificates that are not relevant, the monitor is configured with
a list of domains to match on.  Only matching certificates will be stored, which
means there are nearly no storage requirements to run this type of monitor.

To get the "silent" property, the monitor pulls the trusted systems for
legitimately issued certificates via HTTP GET.  Alternatively, the monitor can
read a local file in case it is co-located with a single trusted system.  The
monitor uses this as [feedback](./feedback.md) to filter the downloaded
certificates that matched.  If a certificate is found that none of the trusted
systems made available, only then is an alert emitted (NOTICE level output).

The communication channel between the trusted systems and the monitor can be
tampered with.  For example, it may be plain HTTP or an HTTPS connection that
the attacker trivially hijacks by obtaining yet another mis-issued certificate.
Owning that the communication channel is insecure helps avoid misconfiguration.

A shared secret is used for each system to authenticate with the monitor.  This
secret is never shown on the wire: an HMAC key is derived from it, which is used
to produce message authentication codes.  All a machine-in-the-middle attacker
can do is replay or block integrity-protected files that a system generated.

"Replays" can happen either way because the monitor polls periodically, i.e.,
the monitor needs to account for the fact that it may poll the same file twice.
Blocking can not be solved by cryptography and would simply result in alerts.

## Related work

The commercial version of `certspotter` supports a push-based method for
[authorizing][] legitimately issued certificates.  The monitor does its
authentication using HTTP tokens.  In contrast, the silentct design is:

  1. Safe against attackers that MitM the communication to the monitor, i.e.,
     message authentication codes are used instead of HTTP access tokens.
  2. Applicable in asynchronous workflows, i.e., the monitor does not need to
     always be online and listen for allowlist requests on a public address.

The initial authors of silentct were not aware of Andrew Ayer's related work
before [this thread](https://follow.agwa.name/notice/AmyLDdYcAqF2p5sG24).

[authorizing]: https://sslmate.com/help/reference/certspotter_authorization_api
