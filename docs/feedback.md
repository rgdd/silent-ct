# Feedback

This document describes the integrity-protected file format that a trusted
system uses when making legitimately issued certificates available to a monitor.

## Format

    NAME MAC
    <CERTIFICATE CHAIN>
    ...
    <CERTIFICATE CHAIN>

`NAME`: identifier that the monitor uses to locate the shared secret.

`MAC`: HMAC with SHA256 as the hash function, computed for line two and forward.
The shared HMAC key is derived as follows by the trusted system and the monitor:

    hkdf := hkdf.New(sha256.New, SECRET, []byte("silentct"), NAME)
    key := make([]byte, 16)
    io.ReadFull(hkdf, key)

`<CERTIFICATE CHAIN>`: certificate chain in PEM format that the trusted system
considers legitimate.  Can be repeated, then delimited by "silentct:separator".
