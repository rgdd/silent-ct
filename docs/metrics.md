# Metrics

`silentct-mon` can output Prometheus metrics -- enable using the `-m` option.

## Examples of useful alerts

  - **The monitor is falling behind on downloading a particular log**, e.g.,
    `silentct_log_size - silentct_log_index > 65536`.
  - **The monitor hasn't seen a fresh timestamp from a particular log**, e.g.,
    `time() - silentct_log_timestamp > 24*60*60`.
  - **The monitor needs restarting**, e.g., `silentct_need_restart != 0`
  - **Unexpected certificates have been found**, e.g.,
    `silentct_unexpected_certificate_count > 0`.

## `"silentct_error_counter"`

```
# HELP silentct_error_counter The number of errors propagated to the main loop.
# TYPE silentct_error_counter counter
silentct_error_counter 0
```

Do not use for alerting, this metric is too noisy and currently used for debug.

## `"silentct_log_index"`

```
# HELP silentct_log_index The next log entry to be downloaded.
# TYPE silentct_log_index gauge
silentct_log_index{log_id="4e75a3275c9a10c3385b6cd4df3f52eb1df0e08e1b8d69c0b1fa64b1629a39df"} 7.30980064e+08
```

`log_id` is a unique log identifier in hex, computed as in RFC 6962 §3.2.

## `"silentct_log_size"`

```
# HELP silentct_log_size The number of entries in the log.
# TYPE silentct_log_size gauge
silentct_log_size{log_id="4e75a3275c9a10c3385b6cd4df3f52eb1df0e08e1b8d69c0b1fa64b1629a39df"} 7.31044085e+08
```

`log_id` is a unique log identifier in hex, computed as in RFC 6962 §3.2.

## `"silentct_log_timestamp"`

```
# HELP silentct_log_timestamp The log's UNIX timestamp in ms.
# TYPE silentct_log_timestamp gauge
silentct_log_timestamp{log_id="4e75a3275c9a10c3385b6cd4df3f52eb1df0e08e1b8d69c0b1fa64b1629a39df"} 1.737202578179e+12
```

`log_id` is a unique log identifier in hex, computed as in RFC 6962 §3.2.

## `"silentct_need_restart"`

```
# HELP silentct_need_restart A non-zero value if the monitor needs restarting.
# TYPE silentct_need_restart gauge
silentct_need_restart 0
```

Restarts are normally not needed; but here's a metric until the `silentct-mon`
implementation can assure that all corner-cases are handled without restarts.

## `"silentct_unexpected_certificate_count"`

```
# HELP silentct_unexpected_certificate_count Number of certificates without any allowlisting
# TYPE silentct_unexpected_certificate_count gauge
silentct_unexpected_certificate_count{crt_sans="example.org www.example.org",log_id="4e75a3275c9a10c3385b6cd4df3f52eb1df0e08e1b8d69c0b1fa64b1629a39df",log_index="1234"} 1
```

`crt_sans` are the subject alternative names in the unexpected certificate,
space separated.

`log_id` is a unique log identifier in hex, computed as in RFC 6962 §3.2.

`log_index` specifies the log entry that contains the unexpected certificate.

See `STATE_DIRECTORY/crt_found/<log_id>-<log_index>.*` for further details.  The
`.json` file contains the downloaded log entry.  The `.ascii` file contains the
parsed leaf certificate in a human-readable format to make debugging easier.

Allowlist an unexpected certificate by ingesting it from a trusted certificate
requester.  Alternatively: stop the monitor, manually move the unexpected
certificate from the "alerting" dictionary to the "legitimate" dictionary in
`STATE_DIRECTORY/crt_index.json`, save, and then start the monitor again.
