package monitor

type Matcher interface {
	// Match determines if a log entry is considered to be a "match" based on
	// some criteria.  An error is returned if any certificate parsing fails.
	Match(leafInput, extraData []byte) (bool, error)
}

type MatchAll struct{}

func (m *MatchAll) Match(leafInput, extraData []byte) (bool, error) {
	return true, nil
}
