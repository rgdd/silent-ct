package flagopt

import (
	"flag"
	"time"
)

func BoolOpt(fs *flag.FlagSet, opt *bool, short, long string, value bool) {
	fs.BoolVar(opt, short, value, "")
	fs.BoolVar(opt, long, value, "")
}

func DurationOpt(fs *flag.FlagSet, opt *time.Duration, short, long string, value time.Duration) {
	fs.DurationVar(opt, short, value, "")
	fs.DurationVar(opt, long, value, "")
}

func UintOpt(fs *flag.FlagSet, opt *uint, short, long string, value uint) {
	fs.UintVar(opt, short, value, "")
	fs.UintVar(opt, long, value, "")
}

func StringOpt(fs *flag.FlagSet, opt *string, short, long, value string) {
	fs.StringVar(opt, short, value, "")
	fs.StringVar(opt, long, value, "")
}
