module rgdd.se/silentct

go 1.22.7

require (
	github.com/google/certificate-transparency-go v1.3.0
	github.com/google/trillian v1.7.0
	github.com/prometheus/client_golang v1.20.5
	github.com/transparency-dev/merkle v0.0.2
	gitlab.torproject.org/rgdd/ct v0.0.0
	golang.org/x/crypto v0.31.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/go-logr/logr v1.4.2 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.55.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	golang.org/x/net v0.31.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20241113202542-65e8d215514f // indirect
	google.golang.org/grpc v1.68.0 // indirect
	google.golang.org/protobuf v1.35.2 // indirect
	k8s.io/klog/v2 v2.130.1 // indirect
)
