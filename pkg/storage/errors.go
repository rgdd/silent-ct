package storage

import "errors"

var ErrorMonitorStateExists = errors.New("monitor state already exists on disk")
