package loglist

import "gitlab.torproject.org/rgdd/ct/pkg/metadata"

// FIXME: helpers that should probably be in the upstream package

func metadataFindLog(md metadata.Metadata, target metadata.Log) bool {
	for _, operator := range md.Operators {
		if findLog(operator.Logs, target) {
			return true
		}
	}
	return false
}

func findLog(logs []metadata.Log, target metadata.Log) bool {
	targetID, _ := target.Key.ID()
	for _, log := range logs {
		id, _ := log.Key.ID()
		if id == targetID {
			return true
		}
	}
	return false
}

func findKey(keys []metadata.LogKey, target metadata.Log) bool {
	targetID, _ := target.Key.ID()
	for _, key := range keys {
		id, _ := key.ID()
		if id == targetID {
			return true
		}
	}
	return false
}

func metadataLogDiff(initial, other metadata.Metadata) (added []metadata.Log, removed []metadata.Log) {
	return metadataNewLogsIn(initial, other), metadataNewLogsIn(other, initial)
}

func metadataNewLogsIn(initial, other metadata.Metadata) (added []metadata.Log) {
	for _, operator := range other.Operators {
		for _, log := range operator.Logs {
			if !metadataFindLog(initial, log) {
				added = append(added, log)
			}
		}
	}
	return
}

func checkLog(log metadata.Log) error {
	return nil // FIXME: check valid key, url, mmd, state
}

func skipLog(log metadata.Log) bool {
	return log.State == nil || // logs without a state are considered misconfigured
		log.State.Name == metadata.LogStatePending || // log is not yet relevant
		log.State.Name == metadata.LogStateRetired || // log is not expected to be reachable
		log.State.Name == metadata.LogStateRejected // log is not expected to be reachable
}
