// Package policy specifies which certificates to look for while monitoring, and
// how to pull legitimately issued certificates from trusted nodes based on a
// shared secret.  Statically configured logs can also be specified, as well as
// logs that should not be monitored even if they appear in any dynamic list.
package policy

import (
	"gitlab.torproject.org/rgdd/ct/pkg/metadata"
)

type Policy struct {
	Monitor Wildcards `json:"monitor"`
	Nodes   []Node    `json:"certificate_requesters"`

	// Optional
	StaticLogs []metadata.Log    `json:"static_logs"`
	RemoveLogs []metadata.LogKey `json:"remove_logs"`
}
