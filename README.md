# silentct

An implementation of a silent Certificate Transparency monitor.

## Status

Drafty prototype, please do not use for anything too serious yet.  See
[RELEASES](./RELEASES) for information on what can be expected right now.

## How it works

A monitor downloads all certificates from Certificate Transparency logs.  This
provides a concise view of which certificates have been issued for what domains.

The monitor is configured to pull certificates from trusted systems that
legitimately request certificates.  The legitimately issued certificates are
typically made available on HTTP(S) URLs that are polled periodically.

To convince the monitor that the file of legitimately issued certificates is
authentic, it is integrity protected using a message authentication code.  So,
the monitor and its trusted systems need to be configured with shared secrets.

What makes this setup "silent" is that the monitor can compute the difference
between any downloaded and legitimately issued certificates.  If a certificate
is found that no trusted system made available, only then an alert is emitted.

See the [silentct design](./docs/design.md) for a lengthier introduction.

## Quickstart

### Setup a trusted system

Install the `silentct-mac` tool.

    $ go install rgdd.se/silentct/cmd/silentct-mac@latest

Mark all certificates that have yet to expire as legitimately issued.  The below
specifies one certificate chain, but it is possible to list multiple ones.

    $ silentct-mac -n example.org -s sikritpassword /etc/letsencrypt/live/example.org/fullchain.pem

`-n` sets an arbitrary name of the trusted system.

`-s` sets a secret that will be shared with the monitor.

The output includes the name, a message authentication code, and the list of
certificate chains that was specified.  Record the output as a file the monitor
can pull, e.g., by saving it in a web root or transferring it to one.  Ensure
that this file gets updated each time a new certificate is legitimately issued.
Below is an example that keeps the file up-to-date with `crontab` and `certbot`.

    # crontab -l
    35 3 * * * certbot renew --post-hook "silentct-mac -n example.org -s sikritpassword -o /var/www/example.org/silentct/allowlist /etc/letsencrypt/live/example.org/fullchain.pem"

**Note:** the `--post-hook` option can be specified per site in the
`[renewalparams]` section of `/etc/letsencrypt/renewal/example.org.conf`.

### Setup the monitor

Install the `silentct-mon` tool:

    $ go install rgdd.se/silentct/cmd/silentct-mon@latest

Create a configuration file.

    $ cat config.json
    {
      "monitor": [
        {
          "bootstrap_at": "2024-01-01T00:00:00Z",
          "suffix": "example.org",
          "excludes": [
            "test"
          ]
        }
      ],
      "certificate_requesters": [
        {
          "name": "example.org",
          "secret": "sikritpassword",
          "location": "https://www.example.org/silentct/allowlist",
          "requests": [
            "example.org",
            "www.example.org"
          ]
        }
      ]
    }

`bootstrap_at` is the time the monitor started looking for certificates that
match `suffix`.  The monitor considers a certificate to match iff (i) it expired
after the bootstrap time, and (ii) at least one subject alternative name ends
with the specified suffix without a longer match being available when taking the
optional `excludes` list of subdomains into account.  For example, the above
configuration matches `www.example.org` but not `foo.test.example.org`.

Each entry in the `"certificate-requesters"` list corresponds to a trusted
system and the domains it requests certificates for.  Set `name`, `secret`,
`location` (filename or URL to pull from), and `requests` to match the
configuration of each trusted system.  The monitor will refuse to mark a
certificate as legitimate unless the trusted system that requested it had
permission to do so.  This adds a layer of separation between trusted systems.

The list of logs is configured and kept up-to-date by downloading [Google's
list][] in signed format.  To remove a log, specify the `"remove_logs"` list.
Each entry should be a log key on the same format as in the signed list.

    "remove_logs": [
        "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8m/SiQ8/xfiHHqtls9m7FyOMBg4JVZY9CgiixXGz0akvKD6DEL8S0ERmFe9U4ZiA0M4kbT5nmuk3I85Sk4bagA=="
    ]

To add or override a log (mostly useful for debug), specify the `"static_logs"`
list.  Each log entry should be on the same format as in the signed list.

[Google's list]: https://github.com/google/certificate-transparency-community-site/blob/master/docs/google/known-logs.md

### Start the monitor

Start the monitor:

    $ silentct-mon -c config.json -d ~/.local/lib/silentct

Use the `--bootstrap` flag when running the monitor for the first time.

Noteworthy events will be printed on stdout using the NOTICE level.  If you
prefer to get the monitor's output in a file, use the `-o` option.

### Stop the monitor

Stop the monitor gracefully by sending the SIGINT or SIGTERM signals.  Nothing
bad will happen on an ungraceful exit (just redundant work on next startup).

## Issue tracker

  - https://git.glasklar.is/rgdd/silentct/-/issues
  - Email rgdd-silentct-issues (at) incoming (dot) glasklar (dot) is

## Contact

  - Room `#certificate-transparency` at OFTC.net
  - Room `#certificate-transparency` at matrix.org
  - Email rgdd (at) glasklarteknik (dot) se
